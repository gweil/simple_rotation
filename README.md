# simple_rotation

WebGL application representing a rotation around a point with [Three.js](https://threejs.org/).

## Usage

### Project setup
```shell
npm install
```

### Compiles and hot-reloads for development
```shell
npm run start
```

### Compiles and minifies for production
```shell
npm run build
```

## Licence

GNU GPLv3
