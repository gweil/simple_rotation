const sceneManager = require('./sceneManager');

// ------------ Global variables ------------

global.WIDTH = window.innerWidth;
global.HEIGHT = window.innerHeight;

let init = () => {
  sceneManager.initScene();
}


let render = () => {
  requestAnimationFrame(render);
  sceneManager.updateScene(WIDTH, HEIGHT);
  sceneManager.renderScene();
}


init();
render();
