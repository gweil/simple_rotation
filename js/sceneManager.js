// three.js
global.THREE = require('three');
require('three/examples/js/controls/OrbitControls');
require('three/examples/js/loaders/OBJLoader');


// ------------ Global variables ------------

global.CLOCK = new THREE.Clock();

const TIME_ANIMATION = 4  // in seconds

let renderer, scene, camera, ratioScene, controls;
let elapsedTime = 0;
let movementVector, movementStartPos;
let targetQuat, newQuat, vecToPoint, rotAxis, angle;

let cow = new THREE.Object3D();

// ------------ Exported functions ------------


export const initScene = () => {
  CLOCK.start();
  ratioScene = WIDTH / HEIGHT;

  movementVector = new THREE.Vector3(-4, -4, 0);
  movementStartPos = new THREE.Vector3(2, 2, 0);
  let rotPoint = new THREE.Vector3(0, 1, 0);  // world coordinates
  angle = 8 * Math.PI;
  rotAxis = new THREE.Vector3(-1, -1, 0).normalize();
  
  let tmp = movementStartPos.clone();
  vecToPoint = rotPoint.clone().sub(tmp);   // transformation for the object to reach the rotation point

  renderer = new THREE.WebGLRenderer({ canvas: output, antialias: true });
  renderer.setSize(WIDTH, HEIGHT);

  scene = new THREE.Scene();
  scene.background = new THREE.Color(0xa0a0a0);

  addLights();

  camera = new THREE.PerspectiveCamera(45, ratioScene, 0.001, 20);
  camera.position.set(0, 2, 10);
  camera.lookAt(0, 0, 0);
  scene.add(camera);

  controls = new THREE.OrbitControls(camera, renderer.domElement);
  controls.minDistance = 0.005;
  controls.maxDistance = 100;

  let ax = new THREE.AxesHelper(1);
  scene.add(ax);

  let objLoader = new THREE.OBJLoader();
  objLoader.load(
    'static/cow.obj',
    object => {
      cow.copy(object);
      cow.position.copy(movementStartPos);
      let tmpAxes = new THREE.AxesHelper(1);
      cow.add(tmpAxes);
      scene.add(cow);
    },
    undefined,
    error => { console.warn(error); }
  );

  targetQuat = new THREE.Quaternion().setFromAxisAngle(rotAxis, angle);
  newQuat = new THREE.Quaternion();
}


export const updateScene = (width, height) => {
  controls.update();

  ratioScene = width / height;
  renderer.setSize(width, height);
  camera.aspect = ratioScene;
  camera.updateProjectionMatrix();

  // ---------- Animation of the object ----------

  let delta = CLOCK.getDelta();

  if (delta + elapsedTime < TIME_ANIMATION) {
    elapsedTime += delta;
    
    let movement = movementVector.clone().multiplyScalar(delta / TIME_ANIMATION);
    cow.position.add(movement);

    cow.translateX(vecToPoint.x);
    cow.translateY(vecToPoint.y);
    cow.translateZ(vecToPoint.z);
    let angleTmp = angle * delta / TIME_ANIMATION;
    newQuat = new THREE.Quaternion().setFromAxisAngle(rotAxis, angleTmp);
    cow.applyQuaternion(newQuat);
    cow.translateX(- vecToPoint.x);
    cow.translateY(- vecToPoint.y);
    cow.translateZ(- vecToPoint.z);
    
  } else {
    let movement = movementVector.clone().multiplyScalar((TIME_ANIMATION - elapsedTime) / TIME_ANIMATION);
    cow.position.add(movement);
    
    cow.translateX(vecToPoint.x);
    cow.translateY(vecToPoint.y);
    cow.translateZ(vecToPoint.z);
    newQuat = new THREE.Quaternion().setFromAxisAngle(rotAxis, angle);
    cow.applyQuaternion(newQuat);
    cow.translateX(- vecToPoint.x);
    cow.translateY(- vecToPoint.y);
    cow.translateZ(- vecToPoint.z);

    elapsedTime = 0;
    updateValues();
  }
}


export let renderScene = () => {
  renderer.render(scene, camera);
}


// ------------ Internal functions ------------


const addLights = () => {
  let hemiLight = new THREE.HemisphereLight(0xffffff);
  hemiLight.groundColor.set(0xb2cdee);
  hemiLight.position.set(0, 10, 0);
  scene.add(hemiLight);

  let pointLight = new THREE.PointLight(0xffffff);
  pointLight.position.set(0, 1, 0);
  scene.add(pointLight);
}


let updateValues = () => {
  movementVector.x === -4 ?
    ( 
      movementVector = new THREE.Vector3(4, 4, 0)
    ) : ( 
      movementVector = new THREE.Vector3(-4, -4, 0)
    );
  targetQuat.inverse();
}
